create table name
(
	nconst text NOT NULL,
	primaryName text,
	birthYear text,
	deathYear text,
	primaryProfession text,
	knownForTitles text,
	PRIMARY KEY(nconst)
);

create table title
(
	tconst text NOT NULL,
	titleType text,
	primaryTitle text,
	originalTitle text,
	isAdult integer,
	startYear integer,
	endYear integer,
	runtimeMinutes integer,
	genres text,
	PRIMARY KEY(tconst)
);

create table title_aka
(
	titleId text NOT NULL
		references title,
	ordering integer NOT NULL,
	title text,
	region text,
	language text,
	types text,
	attributes text,
	isOriginalTitle integer,
	PRIMARY KEY(titleId, ordering)
);

create table title_crew
(
	tconst text NOT NULL
		references title,
	directors text,
	writers text,
	PRIMARY KEY(tconst)
);

create table title_episode
(
	tconst text NOT NULL
		references title,
	parentTconst text NOT NULL
		references title,
	seasonNumber integer,
	episodeNumber integer,
	PRIMARY KEY(tconst)
);

create table title_principals
(
	tconst text NOT NULL
		references title,
	ordering integer NOT NULL,
	nconst text NOT NULL
		references name,
	category text,
	job text,
	characters text,
	PRIMARY KEY(tconst, ordering)
);

create table title_ratings
(
	tconst text NOT NULL
		references title,
	averageRating real,
	numVotes integer,
	PRIMARY KEY(tconst)
);

