#!/bin/bash
files=(name.basics title.akas title.basics title.crew title.episode title.principals title.ratings)
mkdir -p raw
for filename in ${files[@]}; do
  wget -O - https://datasets.imdbws.com/${filename}.tsv.gz | gzip -d -c > raw/${filename}.tsv
done
